package org.innopolis.stream;

import org.innopolis.stream.entity.*;
import org.innopolis.stream.shaymagsumov.KstuBuilder;

import java.util.*;

import static org.innopolis.stream.entity.Subject.*;
import static org.innopolis.stream.utils.EntityCreator.*;

public class Main {

    public static void main(String[] args) {

        init();

    }

    private static List<University> init() {
        University universityOfKazan = new University();
        universityOfKazan.setName("Kazan State University");

        List<Subject> listOfSubjectsForPolitics = new ArrayList<>();
        listOfSubjectsForPolitics.add(Subject.POLITICS);
        listOfSubjectsForPolitics.add(Subject.WARFARE);
        listOfSubjectsForPolitics.add(Subject.ECONOMICS);
        listOfSubjectsForPolitics.add(Subject.GOVERNMENT_MANAGEMENT);
        listOfSubjectsForPolitics.add(Subject.HISTORY);
        listOfSubjectsForPolitics.add(Subject.SOCIAL_STUDIES);

        List<Student> listGoverment = new ArrayList<>();
        List<Student> listMinistry = new ArrayList<>();

        Set<Group> groupSet = new HashSet<>();

        Student stPutin = new Student();
        stPutin.setName("Владимир Путин");
        stPutin.setCourse(5);//
        stPutin.setAge(68);
        stPutin.setSubjects(listOfSubjectsForPolitics);
        listGoverment.add(stPutin);

        Student stMishustin = new Student();
        stMishustin.setName("Михаил Мишустин");
        stMishustin.setCourse(5);//
        stMishustin.setAge(55);
        stMishustin.setSubjects(listOfSubjectsForPolitics);
        listGoverment.add(stMishustin);

        Student stAbramchenko = new Student();
        stAbramchenko.setName("Виктория Абрамченко");
        stAbramchenko.setCourse(5);//
        stAbramchenko.setAge(46);
        stAbramchenko.setSubjects(listOfSubjectsForPolitics);
        listGoverment.add(stAbramchenko);

        Student stGrigorenko = new Student();
        stGrigorenko.setName("Дмитрий Григоренко");
        stGrigorenko.setCourse(5);//
        stGrigorenko.setAge(43);
        stGrigorenko.setSubjects(listOfSubjectsForPolitics);
        listGoverment.add(stGrigorenko);

        Student stGolikova = new Student();
        stGolikova.setName("Татьяна Голикова");
        stGolikova.setCourse(5);//
        stGolikova.setAge(55);
        stGolikova.setSubjects(listOfSubjectsForPolitics);
        listMinistry.add(stGolikova);

        Student stLavrov = new Student();
        stLavrov.setName("Сергей Лавров");
        stLavrov.setCourse(5);//
        stLavrov.setAge(71);
        stLavrov.setSubjects(listOfSubjectsForPolitics);
        listMinistry.add(stLavrov);

        Student stNovak = new Student();
        stNovak.setName("Александр Новак");
        stNovak.setCourse(5);//
        stNovak.setAge(50);
        stNovak.setSubjects(listOfSubjectsForPolitics);
        listMinistry.add(stNovak);

        Student stHusnullin = new Student();
        stHusnullin.setName("Марат Хуснуллин");
        stHusnullin.setCourse(5);//
        stHusnullin.setAge(55);
        stHusnullin.setSubjects(listOfSubjectsForPolitics);
        listMinistry.add(stHusnullin);

        Group groupGoverment = new Group("Группа 5.1", listGoverment);
        Group groupMinstry = new Group("Группа 5.2", listMinistry);

        groupSet.add(groupGoverment);
        groupSet.add(groupMinstry);

        universityOfKazan.setGroups(groupSet);

        Set<Teacher> teachers = new HashSet<>();

        Teacher teacherPolitics = new Teacher();
        teacherPolitics.setName("Уинстон Черчилль");
        teacherPolitics.setAge(147);
        teacherPolitics.setSubjects(listOfSubjectsForPolitics);
        teachers.add(teacherPolitics);

        Teacher teacherEconomics = new Teacher();
        teacherEconomics.setName("Адам Смит");
        teacherEconomics.setAge(298);
        teacherEconomics.setSubjects(listOfSubjectsForPolitics);
        teachers.add(teacherEconomics);

        universityOfKazan.setTeachers(teachers);

        Teacher teacher1 = createTeacher("Дарья Маркова", 52, List.of(LITERATURE, HISTORY));
        Teacher teacher2 = createTeacher("София Яковлева", 26, List.of(ECONOMICS, SOCIAL_STUDIES));
        Teacher teacher3 = createTeacher("Илья Волков", 37, List.of(MATH, PHYSICS, CHEMISTRY));
        List<Subject> subjects =
                getSubjectsFromTeachers(teacher1.getSubjects(), teacher2.getSubjects(), teacher3.getSubjects());
        Group group1 = createGroup("ГР1-1", subjects, List.of(
                createStudent("Матвей Егоров", 1, 18),
                createStudent("Максим Филиппов", 1, 19),
                createStudent("Сергей Калмыков", 1, 17),
                createStudent("Малика Соколова", 1, 18),
                createStudent("Вероника Кузьмина", 1, 19)
        ));
        Group group2 = createGroup("ГР1-2", subjects, List.of(
                createStudent("Анна Васильева", 1, 18),
                createStudent("Марина Филиппова", 1, 19),
                createStudent("Фатима Федотова", 1, 17),
                createStudent("Владимир Киселев", 1, 19),
                createStudent("Алёна Котова", 1, 18)
        ));
        Group group3 = createGroup("ГР1-3", subjects, List.of(
                createStudent("Виктория Зайцева", 1, 18),
                createStudent("Ярослав Розанов", 1, 19),
                createStudent("Денис Кузьмин", 1, 17),
                createStudent("Эмилия Иванова", 1, 10),
                createStudent("Алёна Иванова", 1, 19)
        ));
        University university = createUniversity(
                "Университет №1",
                Set.of(group1, group2, group3),
                Set.of(teacher1, teacher2, teacher3)
        );
        System.out.println(university);

        Teacher vitBfu = createTeacher("Виталий Владимиров", 45, List.of(MATH, PROGRAMMING));
        Teacher denBfu = createTeacher("Денис Коротков", 38, List.of(WARFARE, MEDICINE));
        Teacher antBfu = createTeacher("Антонина Козырева", 48, List.of(LITERATURE, HISTORY));

        List<Subject> subjBfu = getSubjectsFromTeachers(vitBfu.getSubjects(), denBfu.getSubjects(), antBfu.getSubjects());
        Group groupOneBfu = createGroup("2К-8Л", subjBfu, List.of(
                createStudent("Анна Заикина", 2, 19),
                createStudent("Сергей Кузнецов", 2, 19),
                createStudent("Василиса Великова", 2, 20),
                createStudent("Денис Краснощёков", 2, 19),
                createStudent("Юрий Шнягин", 2, 18)
        ));

        Group groupTwoBfu = createGroup("3М-5Д", subjBfu, List.of(
                createStudent("Максим Поваренков", 3, 20),
                createStudent("Анастасия Гридина", 3, 20),
                createStudent("Черняев Андрей", 3, 21),
                createStudent("Павел Морозов", 3, 19),
                createStudent("Надежда Быкова", 3, 22)
        ));

        Group groupThreeBfu = createGroup("5Р-9С", subjBfu, List.of(
                createStudent("Эдвард Гришин", 5, 22),
                createStudent("Надежда Андреева", 5, 23),
                createStudent("Дарья Вежан", 5, 21),
                createStudent("Александр Вахобов", 5, 22),
                createStudent("Евгения Гусакова", 5, 22)
        ));

        University universitiBfu = createUniversity("БФУ им. Канта", Set.of(groupOneBfu, groupTwoBfu,
                groupThreeBfu), Set.of(vitBfu, denBfu, antBfu));

        System.out.println(universitiBfu);

        University kstu = KstuBuilder.build();
        System.out.println(kstu);

        Teacher teacherAntonina = createTeacher("Антонина Иванова", 90, List.of(MECHANICAL_ENGINEERING, CHEMISTRY));
        Teacher teacherAlbert = createTeacher("Альберт Игнатьев", 45, List.of(PHYSICS, CHEMISTRY));
        Teacher teacherRenat = createTeacher("Ренат Свистоплюев", 67, List.of(MATH, PHYSICS, HISTORY));

        List<Subject> subjectsUniversity =
            getSubjectsFromTeachers(teacherAntonina.getSubjects(), teacherAlbert.getSubjects(), teacherRenat.getSubjects());

        Group group4 = createGroup("ГР1-4", subjectsUniversity, List.of(
            createStudent("Фатхалисламов Нияз", 1, 18),
            createStudent("Фотин Андрей", 1, 19),
            createStudent("Паршаков Александр", 1, 17),
            createStudent("Тюриков Павел", 1, 18),
            createStudent("Осетрова Евгения", 1, 19)
        ));
        Group group5 = createGroup("ГР1-5", subjectsUniversity, List.of(
            createStudent("Мансурова Русалина", 1, 18),
            createStudent("Юрков Юрий", 1, 19),
            createStudent("Муравьев Иван", 1, 17),
            createStudent("Киселев Кирил", 1, 19),
            createStudent("Пивоваров Павел", 1, 18)
        ));
        Group group6 = createGroup("ГР1-6", subjectsUniversity, List.of(
            createStudent("Зайцева Надежда", 1, 18),
            createStudent("Зайцев Виктор", 1, 19),
            createStudent("Кирса Денис", 1, 17),
            createStudent("Рукосуев Иван", 1, 20),
            createStudent("Оголдела Матрена", 1, 19)
        ));
        University universityEtu = createUniversity(
            "ЛЭТИ",
            Set.of(group4, group5, group6),
            Set.of(teacherAntonina, teacherAlbert, teacherRenat)
        );
        System.out.println(universityEtu);

        Teacher teacherTatiana = createTeacher("Татьяна Сергеевна", 60, List.of(PHYSICS));
        Teacher teacherMichael = createTeacher("Михаэль Смит", 45, List.of(PROGRAMMING, MATH));
        Teacher teacherNikolay = createTeacher("Николай Чудотворец", 99, List.of(WARFARE, LITERATURE));

        List<Subject> subjectsVlgu =
                getSubjectsFromTeachers(teacherTatiana.getSubjects(), teacherMichael.getSubjects()
                        , teacherNikolay.getSubjects());

        Group groupVlgu1 = createGroup("НТ - 112", subjectsVlgu, List.of(
                createStudent("Марьяна Емельянова", 1, 19),
                createStudent("Анна Пахомова", 1, 20),
                createStudent("Павел Корчагин", 1, 19),
                createStudent("Ирина Бычкова", 1, 18)
        ));
        Group groupVlgu2 = createGroup("ЛТ - 112", subjectsVlgu, List.of(
                createStudent("Эмин Герасимов", 1, 18),
                createStudent("Александр Григорьев", 1, 20),
                createStudent("Лев Романов", 1, 19),
                createStudent("Фёдор Крылов", 1, 19)
        ));
        Group groupVlgu3 = createGroup("МТ - 112", subjectsVlgu, List.of(
                createStudent("Надежда Плотникова", 1, 18),
                createStudent("Иван Гордеев", 1, 19),
                createStudent("Дмитрий Хохлов", 1, 17),
                createStudent("Владимир Панин", 1, 20)
        ));

        University universityVlgu = createUniversity(
                "ВлГУ",
                Set.of(groupVlgu1, groupVlgu2, groupVlgu3),
                Set.of(teacherTatiana, teacherMichael, teacherNikolay)
        );

        return Arrays.asList(universitiBfu, universityEtu, universityVlgu, universityOfKazan, university)
    }

}