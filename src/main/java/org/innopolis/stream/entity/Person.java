package org.innopolis.stream.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class Person {

    protected String name;
    protected int age;
    protected List<Subject> subjects;

}
