package org.innopolis.stream.entity;

import lombok.Data;

import java.util.Set;

@Data
public class University {

    private String name;
    private Set<Group> groups;
    private Set<Teacher> teachers;

}