package org.innopolis.stream.shaymagsumov;

import org.innopolis.stream.entity.*;
import org.innopolis.stream.utils.EntityCreator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class KstuBuilder {
    public static University build() {
        return EntityCreator.createUniversity("Kazan State Technological University",
                buildGroups(), buildTeachers());
    }

    private static Set<Group> buildGroups() {
        return new HashSet<>(Arrays.asList(buildGroup4120(), buildGroup4520()));
    }

    private static Group buildGroup4120() {
        List<Subject> subjects = Arrays.asList(
                Subject.HISTORY, Subject.SOCIAL_STUDIES, Subject.ECONOMICS, Subject.PHYSICS
        );
        List<Student> students = Arrays.asList(
                EntityCreator.createStudent("Отряд Ковбоев", 1, 18),
                EntityCreator.createStudent("Угон Харлеев", 1, 17),
                EntityCreator.createStudent("Камаз Отходов", 1, 19),
                EntityCreator.createStudent("Развод Супругов", 1, 16)
        );
        return EntityCreator.createGroup("4120", subjects, students);
    }

    private static Group buildGroup4520() {
        List<Subject> subjects = Arrays.asList(
                Subject.MATH, Subject.PROGRAMMING, Subject.ECONOMICS, Subject.MECHANICAL_ENGINEERING
        );
        List<Student> students = Arrays.asList(
                EntityCreator.createStudent("Подрыв Устоев", 5, 22),
                EntityCreator.createStudent("Ушат Помоев", 5, 23),
                EntityCreator.createStudent("Поджог Сараев", 5, 21),
                EntityCreator.createStudent("Рекорд Надоев", 5, 22)
        );
        return EntityCreator.createGroup("4520", subjects, students);
    }

    private static Set<Teacher> buildTeachers() {
        return new HashSet<>(Arrays.asList(
                EntityCreator.createTeacher("Творец Кошмаров", 41,
                        Arrays.asList(Subject.MATH, Subject.PHYSICS, Subject.PROGRAMMING)),
                EntityCreator.createTeacher("Тридня Запоев", 63,
                        Arrays.asList(Subject.HISTORY, Subject.WARFARE, Subject.GOVERNMENT_MANAGEMENT)),
                EntityCreator.createTeacher("Мешок Лимонов", 55,
                        Arrays.asList(Subject.CHEMISTRY, Subject.PHYSICS, Subject.MECHANICAL_ENGINEERING))
        ));
    }
}
