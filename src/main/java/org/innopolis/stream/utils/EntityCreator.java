package org.innopolis.stream.utils;

import org.innopolis.stream.entity.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.innopolis.stream.entity.Subject.*;

public class EntityCreator {

    public static University createUniversity(String name, Set<Group> groups, Set<Teacher> teachers) {
        University university = new University();
        university.setName(name);
        university.setGroups(groups);
        university.setTeachers(teachers);
        return university;
    }

    public static Teacher createTeacher(String name, int age, List<Subject> subjectList) {
        Teacher teacher = new Teacher();
        teacher.setName(name);
        teacher.setAge(age);
        teacher.setSubjects(subjectList);
        return teacher;
    }

    public static Group createGroup(String name, List<Subject> subjects, List<Student> students) {
        students.forEach(student -> student.setSubjects(subjects));
        return new Group(name, students);
    }

    public static Student createStudent(String name, int course, int age, List<Subject> subjectList) {
        Student student = new Student();
        student.setName(name);
        student.setCourse(course);
        student.setAge(age);
        student.setSubjects(subjectList);
        return student;
    }

    public static Student createStudent(String name, int course, int age) {
        Student student = new Student();
        student.setName(name);
        student.setCourse(course);
        student.setAge(age);
        return student;
    }

    @SafeVarargs
    public static List<Subject> getSubjectsFromTeachers(List<Subject>... subjects) {
        return Arrays.stream(subjects).flatMap(Collection::stream).collect(Collectors.toList());
    }

    public static List<Subject> getStdSubjects() {
        return List.of(ECONOMICS, HISTORY, SOCIAL_STUDIES, MATH, LITERATURE, PHYSICS, CHEMISTRY);
    }
}
